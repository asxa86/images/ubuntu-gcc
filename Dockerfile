FROM ubuntu:23.10

RUN apt update -y && \
    apt install -y g++-13 && \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-13 10 && \
    update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-13 10 && \
    update-alternatives --install /usr/bin/cc cc /usr/bin/gcc 20 && \
    update-alternatives --set cc /usr/bin/gcc && \
    update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++ 20 && \
    update-alternatives --set c++ /usr/bin/g++
